import firebase from 'firebase';
import { firebaseConfig } from './firebaseConfig';

export const connectData = firebase.initializeApp(firebaseConfig).database().ref('/chat');