import { connectData } from './firebaseConnection';

export const addMessage = (message) => {
    connectData.set(message);
}