import React from 'react';
import { Router } from '@reach/router';

import Connection from './Connection/Connection';
import Chat from './Chat/Chat';
import NotFound from './NotFound';

import './App.css';

const App = () => {
  return (
    <div className="container">
      <Router>
        <Connection path='/'/>
        <Chat path="/chat/:currentUser" />
        <NotFound default/>
      </Router>
    </div>
  );
}

export default App;
