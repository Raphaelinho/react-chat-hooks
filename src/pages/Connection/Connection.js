import React, { Fragment, useState } from 'react';
import { Redirect } from "@reach/router";

import FormBuilder from '../../components/FormBuilder';
import { connection } from '../../forms/connection';

const Connection = () => {

    const [redirection, setRedirection] = useState({status: false, url: ''});
    const [error, setError] = useState(false);

    const handleRedirection = (user) => {
        if(user.nom !== undefined){
            const username = user.nom.trim();
            if(username.length > 0){
                setRedirection({status: true, url: `/chat/${username}`});
            }else {
                setError(true);
                setTimeout(() => setError(false), 3000);
            }      
        }else {
            setError(true);
            setTimeout(() => setError(false), 3000);
        }
    }

    return (
        <Fragment>
            <h1>Connexion</h1>
            <FormBuilder Input={connection} Class="forms" nameButton="Connexion" getDataForm={handleRedirection}/>
            {redirection.status ? <Redirect  to={redirection.url} noThrow={redirection.status}/> : null}
            {error ? "Veuillez renseigner le champs SVP" : null}
        </Fragment>
    )
}

export default Connection;
 