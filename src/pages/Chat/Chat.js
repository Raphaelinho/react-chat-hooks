import React, { useState } from 'react';
import { useParams } from '@reach/router';

import { connectData } from '../../firebase/firebaseConnection'; 
import FirebaseDisplay from '../../components/DatabaseFirebase';
import { chat } from '../../forms/chat';
import FormBuilder from '../../components/FormBuilder';
import { addMessage } from '../../firebase/firebaseAddMessage'

const Chat = () => {
    const { currentUser } = useParams();
    const [ currentMessage, setCurrentMessage ] = useState([]);

    const getAllMsg = (allMsg) => {
        setCurrentMessage(allMsg);
    }

    const sendMessageAtFirebase = (message) => {
        const objectMessage = []

        currentMessage.map(allMsg => {
           objectMessage.push(allMsg.val());
        })
        objectMessage.push({username : currentUser, message: message.chat});

        if(objectMessage.length > 10) {
            objectMessage.splice(0, 1, null);
        }
        addMessage(objectMessage);
    };

    return (
        <div>
            <h1>Bienvenue dans le chat {currentUser}</h1>
            <FirebaseDisplay bdd={connectData} currentUser={currentUser} getDataBDD={getAllMsg}/>
            <FormBuilder Input={chat} getDataForm={sendMessageAtFirebase} nameButton="Envoyer votre message" />
        </div>
    )
}

export default Chat;
