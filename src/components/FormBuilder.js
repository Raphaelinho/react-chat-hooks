import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';

/**
 * @param {Object[]} Input
 * @param {string} Class
 * @param {function} getDataForm
 * @param {string} nameButton
 * Component allow to create form less requets 
 * Four props : Input, Class, getDataForm, nameButton 
 * Input : type Array with any object, an object = input.
 *          The object can have these attributs : required "type" for input type, "nav" for defined the input
 *          , required "name" for the state
 * Class: type String, to give a class for the form
 * getDataForm: type Function, a function setState  
 * nameButton: type String, to give a personalize button name 
 * 
 */

//Need to put some security value object, check input object

const FormBuilder = ({Input, Class, getDataForm, nameButton}) => {

    let displayForm = null;

    const [input, setInput] = useState({});

    const handleInput = (e) => {
        setInput({...input,  [e.target.id] : e.target.value });
    }

    const handleSubmit = () => {
        getDataForm(input);
    }

    if(Array.isArray(Input)){
        displayForm = Input.map((result, key) => {
            const keysInput = Object.keys(result);
            if(keysInput.indexOf('type') !== -1 && keysInput.indexOf('name') !== -1){
                const valuesInput = Object.values(result);
                if(valuesInput.indexOf('email') !== -1 || valuesInput.indexOf('text') !== -1 || valuesInput.indexOf('password') !== 1){
                    return (
                        <Fragment key={key}>
                            {result.nav ? <nav>{result.nav} :</nav> : null}
                            <input id={result.name} type={result.type} value={input[result.name] || ''} onChange={handleInput}/>
                        </Fragment>
                    )
                }else {
                    return new Error("The input type is false");
                }
            } else {
                return new Error("The object dont have the keys type or name");
            }
        })
    } else {
        new Error("ERROR IS'NT A ARRAY");
    }
    

    return (
        <div className={Class}>
            {displayForm ? displayForm : <p>Loading ...</p>}
            <button onClick={handleSubmit}>{nameButton ? nameButton : "Valider"}</button>
        </div>
    )
}

FormBuilder.protoTypes = {
    Input : PropTypes.array,
    Class : PropTypes.string,
    getDataForm : PropTypes.func,
    nameButton : PropTypes.string
}

export default FormBuilder;