import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useList } from 'react-firebase-hooks/database';

/**
 * @param {object} bdd
 * @param {string} currentUser
 * @param {function} getDataBDD
 * @returns {*} HTML 
 * 
 * DatabaseFirebase allow to display data from your database
 * This class have 3 props, bdd is object firebase connect at your database reeltime, currentUser to know how is connected , and
 * getDataBDD to know all data for the method addMessage
 * bdd: this is an object firebase database reeltime with ref()
 * currentUser: Its for choose the class   
 * getDataUserBDD: function get data snapshot
 */


const DatabaseFirebase = ({bdd, currentUser, getDataBDD}) => {
    
    const [ snapshots, loading, error ] = useList(bdd);

    useEffect(() => {
        getDataBDD(snapshots);
        console.log(snapshots.length)
    })    

    return (
        <div className="messages">
            {error && <strong>Error: {error}</strong>}
            {loading && <span>List: Loading...</span>}
            {!loading && snapshots && (
                <React.Fragment>
                {snapshots.length > 0 ? null : "Aucun message"}
                <div className="message">
                    {snapshots.map((message, key) => (
                        <div className={message.val().username === currentUser ? "user-message" : "not-user-message"} key={key}>
                            <span>{message.val().username}</span>
                            <p>{message.val().message}</p>
                        </div>
                    ))}
                </div>
                </React.Fragment>
            )}
        </div>
    )
}

DatabaseFirebase.propTypes = {
    bdd : PropTypes.object,
    currentUser : PropTypes.string,
    getDataBDD : PropTypes.func
}

export default DatabaseFirebase; 